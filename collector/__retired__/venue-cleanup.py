import csv


def extract_unique_types():
    types = []

    with open('output/combined.csv', 'r') as f:
        venues = csv.DictReader(f)

        for venue in venues:
            types.extend(venue['types'].split(','))

        print('\n'.join(set(types)))


def is_accepted_by_type(venue):
    allowed_types = ['bar', 'night_club', 'stadium', 'movie_theater', 'cafe']

    types = venue['types'].split(',')

    for t in types:
        if t in allowed_types:
            return True

    else:
        return False


def is_accepted_by_name(venue):
    allowed_names = ['music', 'concert', 'live', 'theatre']

    for name in allowed_names:
        if name in venue['name'].lower():
            return True

    else:
        return False


def has_unaccepted_name(venue):
    unexpected_names = ['sushi', 'beauty', 'cocktail', 'noodle', 'dining', 'sport', 'restaurant', 'espresso', 'coffee',
                        'fish', 'australia post', 'coles', 'event cinemas', 'furniture', 'harvey norman', 'herbalife',
                        'hoyts', 'jb hi-fi', 'oyster']

    if venue['name'].lower().startswith('BP ') or venue['name'].strip().lower() == 'bp':
        return True

    for name in unexpected_names:
        if name in venue['name'].lower():
            return True
    else:
        return False


def has_unaccepted_type(venue):
    unexpected_types = ['route', 'store', 'street_address', 'school', 'electronics_store']

    types = venue['types'].split(',')

    for t in types:
        if t in unexpected_types:
            return True
    else:
        return False


def cleanup():
    cleaned = []

    with open('output/combined.csv', 'r') as f:
        venues = csv.DictReader(f)

        for venue in venues:
            if (is_accepted_by_name(venue) and not has_unaccepted_type(venue)) or \
                    (is_accepted_by_type(venue) and not has_unaccepted_name(venue)):
                cleaned.append(venue)

    with open('output/cleaned.csv', 'w') as csvfile:
        fieldnames = ['id', 'place_id', 'name', 'address', 'icon', 'lat', 'lng', 'rating', 'types']
        writer = csv.DictWriter(csvfile, fieldnames)

        writer.writeheader()
        writer.writerows(cleaned)

cleanup()
