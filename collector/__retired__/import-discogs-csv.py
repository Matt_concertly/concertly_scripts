import xml.etree.cElementTree as ET
import argparse
import csv


class XmlListConfig(list):
    def __init__(self, aList):
        for element in aList:
            if element:
                # treat like dict
                if len(element) == 1 or element[0].tag != element[1].tag:
                    self.append(XmlDictConfig(element))
                # treat like list
                elif element[0].tag == element[1].tag:
                    self.append(XmlListConfig(element))
            elif element.text:
                text = element.text.strip()
                if text:
                    self.append(text)


class XmlDictConfig(dict):
    """
    Example usage:

    >>> tree = ElementTree.parse('your_file.xml')
    >>> root = tree.getroot()
    >>> xmldict = XmlDictConfig(root)

    Or, if you want to use an XML string:

    >>> root = ElementTree.XML(xml_string)
    >>> xmldict = XmlDictConfig(root)

    And then use xmldict for what it is... a dict.
    """

    def __init__(self, parent_element):
        if parent_element.items():
            self.update(dict(parent_element.items()))
        for element in parent_element:
            if element:
                # treat like dict - we assume that if the first two tags
                # in a series are different, then they are all different.
                if len(element) == 1 or element[0].tag != element[1].tag:
                    aDict = XmlDictConfig(element)
                # treat like list - we assume that if the first two tags
                # in a series are the same, then the rest are the same.
                else:
                    # here, we put the list in dictionary; the key is the
                    # tag name the list elements all share in common, and
                    # the value is the list itself
                    aDict = {element[0].tag: XmlListConfig(element)}
                # if the tag has attributes, add those to the dict
                if element.items():
                    aDict.update(dict(element.items()))
                self.update({element.tag: aDict})
            # this assumes that if you've got an attribute in a tag,
            # you won't be having any text. This may or may not be a
            # good idea -- time will tell. It works for the way we are
            # currently doing XML configuration files...
            elif element.items():
                self.update({element.tag: dict(element.items())})
            # finally, if there are no child tags and no attributes, extract
            # the text
            else:
                self.update({element.tag: element.text})


def u(s):
    return unicode(s).encode('utf-8')


def read_xml(filename):
    tree = ET.parse(filename)
    root = tree.getroot()

    last_percent = 0

    artists = []

    elements = root.findall('artist')
    for idx, artist in enumerate(elements):
        discogs_id = artist.find('id').text

        name = artist.find('name').text if artist.find('name') is not None else ''
        realname = artist.find('realname').text if artist.find('realname') is not None else ''
        name_variations = [variation.text for variation in artist.find('namevariations') or []]
        aliases = [alias.text for alias in artist.find('aliases') or []]
        data_quality = artist.find('data_quality').text
        members = [int(mid.text) for mid in artist.findall('members/id')]
        profile = artist.find('profile').text

        artists.append(dict(
            discogs_id=u(discogs_id),
            artist_name=u(name),
            artist_realname=u(realname),
            data_quality=u(data_quality),
            name_variations=u(name_variations),
            aliases=u(aliases),
            profile=u(profile),
            members=u(members),
        ))

        percent = int(((idx * 1.0) / len(elements)) * 100)
        if last_percent != percent:
            last_percent = percent
            print(percent)

    return artists


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('filename')
    args = parser.parse_args()

    artists = read_xml(args.filename)

    with open('artists.csv', 'w') as f:
        writer = csv.DictWriter(f, [
            'discogs_id', 'artist_name', 'artist_realname', 'data_quality', 'name_variations', 'aliases',
            'profile', 'members',
        ])

        writer.writeheader()
        writer.writerows(artists)


if __name__ == '__main__':
    main()

