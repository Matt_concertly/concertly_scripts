import csv
import glob


filenames = glob.glob('output/venues/*.csv')

combined_venues = []

for filename in filenames:
    with open(filename, 'r') as f:
        venues = csv.DictReader(f)

        combined_venues.extend(venues)

unique_venues = [dict(tupleized) for tupleized in set(tuple(item.items()) for item in combined_venues)]

with open('output/combined.csv', 'w') as f:
    output = csv.DictWriter(f, ['id', 'place_id', 'name', 'address', 'icon', 'lat', 'lng', 'rating', 'types'])

    output.writeheader()
    output.writerows(unique_venues)
