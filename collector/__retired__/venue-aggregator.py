import csv
import os
import sys
import time

import googlemaps
from googlemaps import places

from retired.localities import find_localities_by_postcodes

cities = [
    dict(name='Darwin', pcode=800),
    dict(name='Sydney', pcode=2000),
    dict(name='Canberra', pcode=2600),
    dict(name='Melbourne', pcode=3000),
    dict(name='Brisbane', pcode=4000),
    dict(name='Gold Coast', pcode=4217),
    dict(name='Adelaide', pcode=5000),
    dict(name='Perth', pcode=6000),
    dict(name='Tasmania', pcode=7000),
]


def generate_locality_filename(locality, suffix=None):
    output_path = 'output/venues'
    if not os.path.isdir(output_path):
        os.makedirs(output_path)

    return '%s/%s-%s%s.csv' % (output_path, locality[0], locality[1], '-' + suffix if suffix else '')


def generate_locality_queries(locality):
    address = '%s %s %s' % (locality[1], locality[2], locality[0])
    return [
        dict(name='venue', q='Music Venues Near %s' % address),
        dict(name='theatre', q='Theatres Near %s' % address),
        dict(name='nightlife', q='Nightlife Near %s' % address),
    ]


def u(s):
    return unicode(s).encode('utf-8')


def save_csv(filename, venues):
    with open(filename, 'w') as csvfile:
        fieldnames = ['id', 'place_id', 'name', 'address', 'icon', 'lat', 'lng', 'rating', 'types']
        writer = csv.DictWriter(csvfile, fieldnames)

        writer.writeheader()

        for venue in venues:
            loc = venue['geometry']['location']
            writer.writerow(dict(
                id=u(venue['id']),
                place_id=u(venue['place_id']),
                name=u(venue['name']),
                address=u(venue['formatted_address']),
                icon=u(venue['icon']),
                lat=u(loc['lat']),
                lng=u(loc['lng']),
                rating=u(venue['rating']) if 'rating' in venue else None,
                types=u(','.join(venue['types']))
            ))


def extract_city_venues(city_pcode):
    client = googlemaps.Client(key='AIzaSyByAFgq7F78hKM6wWo0514cMq4qltQ7ekM')

    for pcode in range(city_pcode, city_pcode + 200):

        for locality in find_localities_by_postcodes('%04d' % pcode):

            for query in generate_locality_queries(locality):
                venues = []
                output_filename = generate_locality_filename(locality, query['name'])

                if os.path.isfile(output_filename):
                    print('Skipping "%s"' % query['q'])
                    continue

                print('Processing "%s"' % query['q'])

                response = places.places(client, query['q'])
                venues.extend(response['results'])

                while 'next_page_token' in response:
                    next_page = response['next_page_token']

                    try:
                        response = places.places(client, query['q'], page_token=next_page)
                    except googlemaps.exceptions.ApiError:
                        time.sleep(2)
                        response = places.places(client, query['q'], page_token=next_page)

                    venues.extend(response['results'])

                save_csv(output_filename, venues)

if __name__ == '__main__':
    if len(sys.argv) > 1:
        input_pcode = sys.argv[1]
        extract_city_venues(int(input_pcode))

    else:
        for city in cities:
            extract_city_venues(city['pcode'])

