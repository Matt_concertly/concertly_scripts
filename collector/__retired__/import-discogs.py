from __future__ import print_function

import argparse
import xml.etree.cElementTree as ET

import psycopg2

from events import settings

conn = psycopg2.connect(
    database=settings.DATABASE['database'], user=settings.DATABASE['user'], password=settings.DATABASE['password'],
    host=None, port=None, connection_factory=None, cursor_factory=None, async=False,
)


def read_xml(filename):
    tree = ET.parse(filename)
    root = tree.getroot()

    last_percent = 0

    c = conn.cursor()

    elements = root.findall('artist')
    for idx, artist in enumerate(elements):
        discogs_id = artist.find('id').text

        name = artist.find('name').text if artist.find('name') is not None else ''
        realname = artist.find('realname').text if artist.find('realname') is not None else ''
        name_variations = [variation.text for variation in artist.find('namevariations') or []]
        aliases = [alias.text for alias in artist.find('aliases') or []]
        data_quality = artist.find('data_quality').text
        members = [int(mid.text) for mid in artist.findall('members/id')]
        profile = artist.find('profile').text

        c.execute('''
INSERT INTO artist(discogs_id, artist_name, artist_realname, data_quality, name_variations, aliases, profile, members)
VALUES(%s, %s, %s, %s, %s, %s, %s, %s)
''', (
            discogs_id, name, realname, data_quality, name_variations, aliases, profile, members
        ))

        percent = int(((idx * 1.0) / len(elements)) * 100)
        if last_percent != percent:
            last_percent = percent
            print(percent)

    conn.commit()


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('filename')
    args = parser.parse_args()

    read_xml(args.filename)


if __name__ == '__main__':
    main()
