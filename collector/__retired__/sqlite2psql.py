import sqlite3
import psycopg2


def dict_factory(cursor, row):
    d = {}
    for idx, col in enumerate(cursor.description):
        d[col[0]] = row[idx]
    return d


def get_all_venues_from_sqlite():
    conn = sqlite3.connect('output/venues.db')
    conn.row_factory = dict_factory

    c = conn.cursor()
    c.execute('SELECT * FROM venues;')

    return c.fetchall()


def main():
    conn = psycopg2.connect(database='concertly')
    c = conn.cursor()

    for venue in get_all_venues_from_sqlite():
        c.execute("""
INSERT INTO venue(google_id, google_place_id, name, google_address, fb_address, icon, loc, rating, venue_types, fb_page_id, fb_page_url, fb_has_events, fb_page_name)
VALUES(%s, %s, %s, %s, %s, %s, ST_GeomFromText('POINT(%s %s)'), %s, %s, %s, %s, %s, %s);""",
                  (
                      venue['google_id'],
                      venue['place_id'],
                      venue['name'],
                      venue['address'],
                      venue['fb_address'],
                      venue['icon'],
                      float(venue['lng']),
                      float(venue['lat']),
                      float(venue['rating']) if venue['rating'] else None,
                      map(unicode.strip, venue['types'].split(',')),
                      venue['fb_pageid'],
                      venue['fb_link'],
                      True if venue['fb_hasevents'] == 1 else False,
                      venue['fb_pagename'],
                  ))

    conn.commit()


if __name__ == '__main__':
    main()
