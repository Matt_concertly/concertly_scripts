from selenium import webdriver
from selenium.webdriver.common.keys import Keys
import selenium.common.exceptions as sexceptions
from urllib2 import urlopen
import sqlite3
import json
import re
import time
import sys


def dict_factory(cursor, row):
    d = {}
    for idx, col in enumerate(cursor.description):
        d[col[0]] = row[idx]
    return d

driver = None


def login():
    global driver
    driver = webdriver.Firefox()
    driver.implicitly_wait(3)
    driver.get('http://www.facebook.com')

    user = driver.find_element_by_id('email')
    user.clear()
    user.send_keys('mamouri@gmail.com')

    password = driver.find_element_by_id('pass')
    password.send_keys('VEGan2550@Sydney')
    password.send_keys(Keys.RETURN)
    time.sleep(10)


def parse_locality(locality):
    regex_locality = re.compile(r'(.*)(AAT|ACT|HIMI|JBT|NSW|NT|QLD|SA|TAS|VIC|WA)\s*(\d{4})')

    locality = regex_locality.findall(locality)

    if locality:
        locality = locality[0]
        locality = map(unicode.strip, locality)

    return locality


def parse_address(address):
    parts = address.split(',')
    parts = map(unicode.strip, parts)

    regex_address = re.compile(r'(.*)(AAT|ACT|HIMI|JBT|NSW|NT|QLD|SA|TAS|VIC|WA)\s*(\d{4})')

    locality_info = None
    for part in parts:
        if regex_address.match(part):
            locality_info = part
            break

    if locality_info is not None:
        return parse_locality(locality_info)
    else:
        return parse_locality(address)


def word_exists(s, words):
    """
    Check if any of the words contain in the text
    :param text: Text to search
    :param words: A list of words to search in the text
    :return: If any of the words exists in the text

    @type s: str or unicode
    @type words: list[str]
    @rtype: bool
    """
    return any(len(re.findall(r'(?:\b|\d)%s\b' % word, s, flags=re.I)) > 0 for word in words)


def find_best_match_fbpage(fbpages, address):
    locality = map(unicode.lower, parse_address(address))

    if not locality:
        return None

    suburb = locality[0]
    state = locality[1]
    postcode = locality[2]

    for fbpage in fbpages:
        fbpage_address = fbpage['address'].lower()

        if suburb in fbpage_address or postcode in fbpage_address:
            return fbpage
    else:
        for fbpage in fbpages:
            fbpage_address = fbpage['address'].lower()

            if state == 'act' and word_exists(fbpage_address, ['australian capital territory', 'act']):
                return fbpage

            if state == 'nsw' and word_exists(fbpage_address, ['new south wales', 'nsw']):
                return fbpage

            if state == 'nt' and word_exists(fbpage_address, ['northern territory', 'nt']):
                return fbpage

            if state == 'qld' and word_exists(fbpage_address, ['queensland', 'qld']):
                return fbpage

            if state == 'sa' and word_exists(fbpage_address, ['south australia', 'sa']):
                return fbpage

            if state == 'tas' and word_exists(fbpage_address, ['tasmania', 'tas']):
                return fbpage

            if state == 'vic' and word_exists(fbpage_address, ['victoria', 'vic']):
                return fbpage

            if state == 'wa' and word_exists(fbpage_address, ['western australia', 'wa']):
                return fbpage
        else:
            return None


def find_page(venue):
    search = driver.find_element_by_class_name('_1frb')
    search.clear()
    search.send_keys(venue['name'])
    driver.switch_to.default_content()
    time.sleep(3)

    # Collect all pages
    fbpages = []

    elements = driver.find_elements_by_xpath('//descendant::*[@class="_21c"]/descendant::*[@class="_205"]')

    if not elements:
        return None

    for element in elements:
        link = element.find_element_by_xpath('descendant::a[1]')
        name = element.find_element_by_class_name('fragmentEnt')
        address = element.find_element_by_xpath('descendant::*[@class="_53aa"]/span')

        fbpages.append(dict(name=name.text, address=address.text, link=link.get_attribute('href')))

    best_page = find_best_match_fbpage(fbpages, venue['address'])

    return best_page


def find_facebook_pages():

    start = sys.argv[1]
    to = sys.argv[2]

    conn = sqlite3.connect('output/venues.db')
    conn.row_factory = dict_factory

    c = conn.cursor()
    c.execute('SELECT * FROM venues WHERE fb_processed IS NULL OR fb_processed = 0 LIMIT %s, %s;' % (start, to))

    rows = c.fetchall()

    for row in rows:
        page = find_page(row)

        if page is not None:
            print(json.dumps(row, indent=4))
            print(json.dumps(page, indent=4))
            print('==================================================')
            c.execute(
                '''UPDATE venues SET fb_processed=1, fb_pagename=:pagename, fb_address=:address, fb_link=:link WHERE id=:aid;''',
                dict(pagename=page['name'], address=page['address'], link=page['link'], aid=row['id'])
            )

            conn.commit()

        else:
            c.execute(
                '''UPDATE venues SET fb_processed=1 WHERE id=:aid;''',
                dict(aid=row['id'])
            )

            conn.commit()

    c.close()


def if_page_supports_events(fb_link):
    driver.get(fb_link)

    try:
        driver.find_element_by_xpath('//*[text()="Events"]').click()
    except (sexceptions.NoSuchElementException, sexceptions.ElementNotVisibleException):
        return False

    # script_elem = driver.find_element_by_xpath('//script[@type="application/ld+json"]')
    # script_src = script_elem.get_attribute('innerHTML')

    # data = json.loads(script_src)

    return True


def find_event_pages():
    start = sys.argv[1]
    to = sys.argv[2]

    conn = sqlite3.connect('output/venues.db')
    conn.execute('PRAGMA busy_timeout = 100000')
    conn.row_factory = dict_factory

    c = conn.cursor()
    c.execute('SELECT * FROM venues WHERE fb_link IS NOT NULL AND fb_hasevents IS NULL LIMIT %s, %s;' % (start, to))

    rows = c.fetchall()

    for row in rows:
        event_support = if_page_supports_events(row['fb_link'])

        c.execute(
            '''UPDATE venues SET fb_hasevents=:support WHERE id=:aid;''',
            dict(support=1 if event_support else 0, aid=row['id'])
        )

        print(row['fb_link'])
        print(event_support)
        print('--------------------------------------------------')
        time.sleep(2)

        conn.commit()

    conn.commit()

    c.close()


def find_facebook_page_ids():
    start = sys.argv[1]
    to = sys.argv[2]

    conn = sqlite3.connect('output/venues.db')
    conn.execute('PRAGMA busy_timeout = 100000')
    conn.row_factory = dict_factory

    c = conn.cursor()
    c.execute('SELECT * FROM venues WHERE fb_hasevents = 1 AND fb_pageid IS NULL LIMIT %s, %s;' % (start, to))

    rows = c.fetchall()
    for row in rows:
        driver.get(row['fb_link'])
        matches = re.findall(r'"pageID":\s*"(.*?)"', driver.page_source, re.I)

        if not matches:
            print('No Matches found for %s' % row['fb_link'])
            print('--------------------------------------------------')
            continue

        print(row['fb_pagename'])
        print(matches[0])
        print('--------------------------------------------------')

        c.execute(
            '''UPDATE venues SET fb_pageid=:pageid WHERE id=:aid;''',
            dict(pageid=matches[0], aid=row['id'])
        )

        conn.commit()

    c.close()


login()
# find_facebook_pages()
# find_event_pages()
find_facebook_page_ids()


"""
def old_ghost_way():
    from ghost import Ghost
    ghost = Ghost()

    with ghost.start() as session:
        page, extra_resources = session.open('https://www.facebook.com/login')

        session.evaluate('document.getElementById("email").value = "mamouri@gmail.com"')
        session.evaluate('document.getElementById("pass").value = "VEGan2550@Sydney"')
        session.call('#loginbutton', 'click', expect_loading=True)
        session.wait_for_page_loaded()

        session.capture_to('tmp_login.png')

        location = 'Gerald%27s%20Bar'
        session.evaluate('window.location.href = "https://www.facebook.com/search/top/?q=%s"' % location)

        # session.evaluate('document.getElementsByClassName("_1frb")[0].value = "%s"' % location)
        # session.click('button')

        session.wait_for_page_loaded()
        session.wait_for_text('Places', timeout=120)
        session.sleep(100)

        session.capture_to('tmp.png')

        with open('tmp.html', 'w') as f:
            f.write(session.content)
"""
