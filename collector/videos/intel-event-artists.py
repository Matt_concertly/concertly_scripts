# coding: utf-8

import re
import string
from itertools import groupby

import psycopg2
from nltk import word_tokenize
from nltk.corpus import stopwords
from nltk.tag import pos_tag
from psycopg2.extras import DictCursor

from events import settings


def find_occurance(sequence, pattern):
    """
    Find occurance of a pattern in a Part Of Speech parsed list.

    :param sequence: A sequence of parsed tokens by pos_tag function.
    :param pattern: A pattern of Part of Speech to search
    :return: The words in the original text
    """
    sequence = zip(*sequence)
    words = sequence[0]
    tags = sequence[1]

    for idx, tag in enumerate(tags):
        extract = tags[idx:idx+len(pattern)]

        if ','.join(extract) == ','.join(pattern):
            return words[idx:idx+len(pattern)]


def remove_potential_country_names(s):
    countries = ['USA', 'Australia' 'Germany', 'UK', 'French', 'Canada', 'South Africa', 'Dutch', 'Netherlands',
                 'New Zealand', 'Indonesia', 'China', 'Russia']
    return re.sub(r'(\(\s*(%s)\))' % '|'.join(countries), '', s, re.I)


def remove_potential_city_names(s):
    cities = ['Gold Coast', 'Sydney', 'Melbourne', 'Alice Springs', 'Adelaide', 'Perth', 'Darwin', 'Brisbane',
              'Canberra', 'NewCastle', 'Wollongong', 'Hunter Valley', 'Tasmania', 'Wagga Wagga', 'Albury',
              'Centeral Coast', 'Cairns', 'Sunshine Coast', 'Broome', ]
    return re.sub(r'(\(\s*(%s)\))' % '|'.join(cities), '', s, re.I)


def find_artists_from_title(title):
    """
    Try to find the artist / band name from the title of the event.

    :param title: Title of the event
    :return:
    """
    title = remove_potential_country_names(title)

    stops = stopwords.words('english') + list(string.punctuation)
    words = [word for word in word_tokenize(title.lower()) if word not in stops]

    if len(words) <= 2:
        return [title.strip()]

    # Try to find the titles that has a with in it (Example: Dallas Frasca - Jive w/ Filthy Lucre + Babes are wolves)
    matches = re.findall(r'(.*)\s*(?:w/|with)\s*(\w+\s*\w+)', title)
    if matches:
        return list(matches[0])

    # Try to find artist name at location string
    matches = re.findall(r'(.*)\s+at\s+.*$', title)
    if matches:
        return [matches[0].strip()]

    # Try to find artist from event that has a dash in them (Example: Harry Potter - Sydney Metro Theatre)
    parts = title.split('-')
    words = [word for word in word_tokenize(parts[0].lower()) if word not in stops]

    if len(words) <= 2:
        return [parts[0].strip()]

    # Try to find names before paranthesis. Example: Po Childs (Lone Stranger)
    matches = re.findall(r'(.*)\s*\(.*\)', title)
    if matches:
        return [matches[0]]


def find_artists_from_text(s):
    parsed = pos_tag(word_tokenize(s))

    patterns = [
        dict(pattern=['NNP', 'NNP', 'VBP'], start=0, stop=2, confident=0.8),
        dict(pattern=['NNP', 'NNP', 'VBZ'], start=0, stop=2, confident=0.8),
        dict(pattern=['NNS', 'NNP', 'VBZ'], start=0, stop=2, confident=0.8),
        dict(pattern=['DT', 'NNP', 'NNP'], start=0, stop=3, confident=0.8),
        dict(pattern=['NNP', 'NNP', 'NNP'], start=0, stop=3, confident=0.8),
        dict(pattern=['NNP', 'NNP'], start=0, stop=2, confident=0.8),
        dict(pattern=['NNP', 'NNP'], start=0, stop=2, confident=0.8),
        dict(pattern=['NNP', 'VBZ'], start=0, stop=1, confident=0.6),
        # dict(pattern=['DT', 'NNS'], start=0, stop=2, confident=0.5),  # Example: The Monkees
    ]

    matches = []
    for pattern in patterns:
        occurance = find_occurance(parsed, pattern['pattern'])
        if occurance is not None:
            occurance = occurance[pattern['start']:pattern['stop']]
            matches.append(dict(phrase=' '.join(occurance), confident=pattern['confident']))

    return matches


def cleanup_description(s):
    """
    Cleaning up Description from unwanted phrases
    :param s:
    :return:
    """
    s = remove_potential_city_names(s)

    return s


def find_event_artists(event):
    """
    Extract list of artists from the even info
    :param event:
    :return:
    """
    artists = []

    matches = find_artists_from_title(event['event_name'])
    if matches is not None:
        artists.extend([dict(name=match, confident=1) for match in matches])

    if event['event_name']:
        matches = find_artists_from_text(event['event_name'].decode('utf-8'))
        artists.extend([dict(name=match['phrase'], confident=(0.9 + match['confident']) / 2) for match in matches])

    if event['description']:
        matches = find_artists_from_text(cleanup_description(event['description']).decode('utf-8'))
        artists.extend([dict(name=match['phrase'], confident=(0.8 + match['confident']) / 2) for match in matches])

    # Sort based on confident level ascindingly
    artists.sort(key=lambda i: i['confident'], reverse=True)

    # Count the duplicates and remove them
    artists = [list(group[1])[0] for group in groupby(artists, lambda artist: artist['name'].lower())]

    return artists


def main():
    """
    This function is only used for development and testing purposes.
    :return:
    """
    conn = psycopg2.connect(
        database=settings.DATABASE['database'], user=settings.DATABASE['user'], password=settings.DATABASE['password'],
        host=settings.DATABASE['host'], port=settings.DATABASE['host'])

    c = conn.cursor(cursor_factory=DictCursor)
    c.execute('SELECT * FROM event WHERE intel_is_music_event=true;')

    events = c.fetchall()

    for event in events:
        artists = find_event_artists(event)

        print('TITLE: %s' % event['event_name'])
        print('DESCRIPTION: %s' % event['description'])
        print('ARTISTS: %s' % artists)
        print('--------------------------------------------------\n\n\n')
        # find_artists(sample5.decode('utf-8'))

if __name__ == '__main__':
    main()

    """
    text = '''
    with special guests ECCA VANDAL & SOX

Fresh off the back of performing two electric sets at BIGSOUND 2016, today we are psyched to announce that the thrash party duo will be heading out on tour in December for their biggest headline shows to date playing a combination of all ages, under 18 and 18+ shows across the country.

Although 2016 was technically supposed to be “a year off to write the new record” for DZ Deathrays, Shane Parsons and Simon Ridley have somehow managed to fit in an insane amount. The duo released their ferocious single ‘Blood On My Leather’, supported Violent Soho on a huge national tour, played Groovin The Moo festival and managed to squeeze in two tours through North America. They’ll see 2016 out touring through the UK, Europe and North America on a co-headline tour with buddies Dune Rats.

As they continue to perfect their third studio album under the helm of producer and friend Burke Reid (of Gerling), we are gifted with ‘Pollyanna’ to tie us over. Featuring all the things that fans and critics have come to love about the two friends’ music, ‘Pollyanna’ sees DZ Deathrays continue their musical exploration as they develop their sound which ultimately has produced a hugely contagious track that will have you bouncing off the walls.

Having been a favourite of the Australian touring circuit for sometime now, these gigs will see the two lads from Bundy turn things up to 11 and with all age groups covered no one has to miss out! Along for the ride will be the insanely cool Ecca Vandal, who is running hot after impressing at this year’s BIGSOUND where she also joined DZ onstage to cover Beastie Boys’ ‘Sabotage’.
'''
    print(pos_tag(word_tokenize(text.decode('utf-8'))))
    """
