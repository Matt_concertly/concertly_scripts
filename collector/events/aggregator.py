from __future__ import print_function
from datetime import datetime
from urllib2 import urlopen, HTTPError
from dateutil.parser import parse
from psycopg2 import connect
from psycopg2.extras import DictCursor
from images import download_image
import settings
import json
import re

conn = connect(
    database=settings.DATABASE['name'],
    user=settings.DATABASE['user'],
    password=settings.DATABASE['password'],
    host=settings.DATABASE['host'],
)


def get_access_token():
    """
    Retrieve an access token from facebook using Facebook App ID and Facebook App Secret.
    Technically access token remain permanant, however we retrieve the access token every time the script runs, just in
    case the access token refreshed by Facebook

    @rtype: int
    """
    url = 'https://graph.facebook.com/oauth/access_token?' + \
          'client_id=%s&client_secret=%s&grant_type=client_credentials' % \
          (settings.FB_APP_ID, settings.FB_APP_SECRET)
    try:
        response = urlopen(url)
        output = response.read()
        return output.split('=')[1]
    except:
        raise


def get_new_events(events):
    """
    Filter a list of events to the events that are occurring after the current date.

    This function remove the timezone from current date to allow comparison between datetime-native and datetime-aware
    formats. Technically this should not cause any problem because we are running the script every day for the events
    and usually the events planned way beforehand.

    @rtype: list
    """

    new = []

    for event in events:
        if parse(event['start_time']).replace(tzinfo=None) > datetime.now():
            new.append(event)

    return new


def get_page_events(access_token, page_id):
    """
    Recursively goes through the Facebook Graphs (Page Events) API and collect all the events of a given page ID.

    :param access_token: The access token to be used with the API
    :param page_id: The page ID of the page that we want to extract
    :return:
    """

    events = []

    # print('https://graph.facebook.com/v2.7/%s/events?access_token=%s' % (page_id, access_token))
    try:
        response = urlopen('https://graph.facebook.com/v2.7/%s/events?access_token=%s' % (page_id, access_token))
    except HTTPError as e:
        result = json.loads(e.read())
        raise Exception('Error downloading events for page %s. Facebook graph error:\n%s' %
                        (page_id, result['error']['message']))

    result = json.loads(response.read())

    new_events = get_new_events(result['data'])
    events.extend(new_events)

    while new_events and 'next' in result['paging']:
        response = urlopen(result['paging']['next'])
        result = json.loads(response.read())

        new_events = get_new_events(result['data'])
        events.extend(new_events)

    return events


def word_exists(s, words):
    """
    Check if any of the words contain in the text
    :param text: Text to search
    :param words: A list of words to search in the text
    :return: If any of the words exists in the text

    @type s: str or unicode
    @type words: list[str]
    @rtype: bool
    """
    if type(s) not in [str, unicode] or type(words) is not list:
        print('==================================================')
        print(s)
        print(words)
    return any(len(re.findall(r'(?:\b|\d)%s\b' % word, s, flags=re.I)) > 0 for word in words)


def is_event_music_event(event_name, description):
    keywords = ['Music', 'DJ', 'DJs', 'Jazz', 'Hip Hop', 'Hip-Hop', 'Rap', 'Rock', 'Rock and roll', 'R&R',
                'Dance', 'Pop', 'Electro', 'Electronica', 'Blues', 'Blues-Rock', 'Opera', 'Indie', 'Metal', 'Funk',
                'Acoustic', 'Guitarist', 'Guitar', 'Concert', 'Piano', 'EDM', 'Techno', 'Punk', 'Folk', 'Tance',
                'Instrumental', "Drum'n'bass", 'Drum and bass', 'Dub', 'Dubstep', 'Ragga', 'Rave', 'R&B', ]

    return word_exists(event_name, keywords) or word_exists(description or '', keywords)


def find_coverphoto(access_token, event):
    event['image'] = None

    # Finding the id of the cover photo
    try:
        response = urlopen('https://graph.facebook.com/v2.8/%s?access_token=%s&fields=cover' %
                           (event['id'], access_token))
        response = json.loads(response.read())
    except HTTPError as e:
        result = json.loads(e.read())
        raise Exception('Downloading cover photo for event %s failed. Facebook graph error:\n%s' %
                        (event['id'], result['error']['message']))

    # If no cover detected return
    try:
        cover_id = response['cover']['id']
    except KeyError:
        return event

    # Get the source of cover image itself
    try:
        response = urlopen('https://graph.facebook.com/v2.8/%s?access_token=%s&fields=images' %
                           (cover_id, access_token))
        response = json.loads(response.read())
    except HTTPError as e:
        result = json.loads(e.read())
        raise Exception('Downloading images of cover image %s failed. Facebook graph error:\n%s' %
                        (cover_id, result['error']['message']))

    # Pick the largest image
    try:
        images = sorted(response['images'], key=lambda x: x['width'], reverse=True)
        event['image'] = images[0]['source']

    except (KeyError, IndexError):
        pass

    return event


def find_covers(access_token, events):
    return [find_coverphoto(access_token, event) for event in events]


def save_venue_events(venue_id, events):
    """
    Save events of a given venue.

    If the event already exit we update the information, otherwise we insert the event into the database.

    :param venue_id: The Venue ID that events belongs to
    :param events: List of events
    :return: None
    """

    c = conn.cursor()

    for event in events:

        event_id = event['id']
        event_name = event['name']
        start_time = parse(event['start_time'])
        description = event['description'] if 'description' in event else None

        lng = 0
        lat = 0
        place = event['place'] if 'place' in event else None

        try:
            location = place['location']
            lng = location['longitude']
            lat = location['latitude']
        except (KeyError, TypeError):
            pass

        c.execute('''
INSERT INTO event(venue_id, event_id, event_name, start_time, place, loc, description, intel_is_music_event)
VALUES(%s, %s, %s, %s, %s, ST_GeomFromText('POINT(%s %s)'), %s, %s)
ON CONFLICT (venue_id, event_id)
DO UPDATE SET event_name=%s, start_time=%s, place=%s, loc=ST_GeomFromText('POINT(%s %s)'), description=%s,
    intel_is_music_event=%s;
        ''', (
            venue_id, event_id, event_name, start_time, json.dumps(place), lng, lat, description,
            is_event_music_event(event_name, description),

            event_name, start_time, json.dumps(place), lng, lat, description,
            is_event_music_event(event_name, description),
        ))

    conn.commit()


def get_venues_to_process():
    """
    Take ten events form the database to process and mark them as processed.

    This function does this by locking rows that is updating. Therefore it allows us to run concurrent events
    aggregator.

    :return:
    """

    c = conn.cursor(cursor_factory=DictCursor)

    c.execute("""
UPDATE venue s
SET last_checked_for_new_events = NOW()
FROM (
  SELECT id, name, last_checked_for_new_events
  FROM venue
  WHERE venue.fb_has_events = true AND
        venue.fb_page_id IS NOT NULL AND
        last_checked_for_new_events IS NULL OR last_checked_for_new_events < NOW()
  ORDER BY venue.last_checked_for_new_events ASC NULLS FIRST
  LIMIT 100
  FOR UPDATE SKIP LOCKED
) sub
WHERE s.id = sub.id
RETURNING s.id, s.fb_page_id;
    """)

    return c.fetchall()


def main():
    access_token = get_access_token()

    venues = get_venues_to_process()

    for venue in venues:
        print('[INFO] Extracting events from venue %d' % venue['id'])
        try:
            events = get_page_events(access_token, venue['fb_page_id'])
            events = find_covers(access_token, events)
            events = [download_image(event) for event in events]
        except Exception as e:
            print(e.message + '\n')
            continue

        # print(json.dumps(events, indent=4))
        save_venue_events(venue['id'], events)

        print()


if __name__ == '__main__':
    main()
