from datetime import datetime, timedelta
from hashlib import md5
from urllib2 import urlopen
from PIL import Image, ImageOps
from mimetypes import guess_extension
import settings
import boto3
import io

aws_session = boto3.session.Session(
    aws_access_key_id=settings.AWS['access_key'],
    aws_secret_access_key=settings.AWS['secret'],
    region_name=settings.AWS['region']
)
s3 = aws_session.resource('s3')


def calc_hash(s):
    phash = md5()
    s.seek(0)
    phash.update(s.read())
    return phash.hexdigest()


def create_thumbnail(fd):
    out = io.BytesIO()

    img = Image.open(fd)
    img = ImageOps.fit(img, (200, 200), centering=(0.1, 0.3))
    img.save(out, 'JPEG')

    return out


def create_cover(fd):
    out = io.BytesIO()

    img = Image.open(fd)
    img = ImageOps.fit(img, (600, int(600 / 1.90)), centering=(0, 0))
    img.save(out, 'JPEG')

    return out


def create_marker(fd):
    mask = Image.open('../assets/marker.png').convert('L')

    cut = ImageOps.fit(Image.open(fd), mask.size, centering=(0.5, 0.5))
    cut.putalpha(mask)
    cut = cut.resize(map(lambda x: x - 50, cut.size))

    stroke = Image.open('../assets/stroke.png')
    x, y = 25, 25
    w, h = cut.size
    stroke.paste(cut, (x, y, x + w, y + h))
    stroke.save('marker-stroke.png')

    return stroke


def upload_file(filename, fd, content_type):
    remote_filename = 'events/' + filename

    fd.seek(0)
    s3.Object(settings.BUCKET, remote_filename).put(
        Body=fd.read(),
        ContentType=content_type,
        Expires=datetime.now() + timedelta(weeks=365 / 7 * 10)
    )

    return remote_filename


def download_image(event):
    """
    Download the cover image for the event
    :param event:
    :return:
    """
    if not event['cover']:
        return event

    response = urlopen(event['cover'])
    content_type = response.headers.get('content-type')
    ext = guess_extension(content_type)
    if ext == '.jpe':
        ext = '.jpg'

    original_image = io.BytesIO(response.read())
    fhash = calc_hash(original_image)

    original_filename = upload_file('%s%s' % (fhash, ext), original_image, content_type)
    thumnail_filename = upload_file('%s_thumb%s' % (fhash, ext), create_thumbnail(original_image), content_type)
    cover_filename = upload_file('%s_cover%s' % (fhash, ext), create_cover(original_image), content_type)

    create_marker(original_image)

    event['image'] = dict(
        filename=original_filename,
        thumnail=thumnail_filename,
        cover=cover_filename,
        # marker=create_marker()
    )

    return event


# mock_event = dict(cover='http://www.w3schools.com/css/trolltunga.jpg')
# print(download_image(mock_event))

with open('a.jpg', 'r') as f:
    create_marker(f)


